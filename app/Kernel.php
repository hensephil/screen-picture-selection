<?php

namespace App;

use PetrKnap\Php\Singleton\SingletonTrait;
use Symfony\Component\HttpFoundation\Request;

class Kernel
{
    use SingletonTrait;

    /**
     * App constructor.
     */
    public function __construct()
    {
        session_start();

        DB::getInstance();

        Template::getInstance();

        Router::getInstance();
    }

    /**
     * @return string
     */
    public static function getRootPath(): string
    {
        return dirname(__DIR__);
    }

    /**
     * @return string
     */
    public static function getPublicRelativePath(): string
    {
        $request = Request::createFromGlobals();

        return str_replace('/index.php', '', $request->getScriptName());
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $message
     * @param array $additionalHeaders
     * @param null $additionalParams
     *
     * @return bool
     */
    public static function sendMail(string $from, string $to, string $subject, string $message, array $additionalHeaders = [], $additionalParams = null): bool
    {
        $headers = array_replace_recursive([
            'From' => $from,
        ], $additionalHeaders);

        return mail(
            $to,
            $subject,
            $message,
            implode("\r\n", array_map(fn ($key, $value) => "$key: $value", array_keys($headers), $headers)),
            $additionalParams
        );
    }
}
