<?php

namespace App\Controllers;

use App\DB;
use App\Models\User;
use App\Router;
use App\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Account
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public static function index(Request $request): string
    {
        $user = User::getFromSession();

        $context = [
            'username' => $user->username,
            'email' => $user->email,
        ];

        return Template::getInstance()->render('account.twig', $context);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public static function update(Request $request): Response
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');

        $user = User::getFromSession();
        $user->username = $username;
        $user->email = $email;
        if (! empty($password)) {
            $user->password = password_hash($password, PASSWORD_DEFAULT);
        }
        DB::getInstance()->save($user);

        return new RedirectResponse(Router::route('/account'));
    }
}
