<?php

namespace App\Controllers;

use App\Template;
use Symfony\Component\HttpFoundation\Request;

class Dashboard
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public static function dashboard(Request $request): string
    {
        return Template::getInstance()->render('dashboard.twig');
    }
}
