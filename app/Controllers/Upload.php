<?php

namespace App\Controllers;

use App\DB;
use App\Kernel;
use App\Models\Image;
use App\Models\Setting;
use App\Models\User;
use App\Router;
use App\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class Upload
{
    /**
     * @return string
     */
    public static function form(): string
    {
        return Template::getInstance()->render('upload.twig');
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public static function upload(Request $request)
    {
        $images = $request->files->get('images');
        foreach ($images as $image) {
            /**@var UploadedFile $image*/

            $file = $image->move(self::getFolderPathForType('new'), time() . '-' . $image->getClientOriginalName());

            $dbImage = new Image();
            $dbImage->path = $file->getRealPath();
            $dbImage->status = 'new';
            $dbImage->processedBy = $_SESSION['user'] ?? null;
            DB::getInstance()->save($dbImage);

            // send mail to all users
            $userRepository = DB::getInstance()->entityManager->getRepository(User::class);
            $users = $userRepository->findAll();
            foreach ($users as $user) {
                Kernel::sendMail(Setting::get('email/new_images/from'), $user->email, Setting::get('email/new_images/subject'), Setting::get('email/new_images/message'));
            }
        }

        return new RedirectResponse(Router::route('/upload'));
    }

    /**
     * @return string
     */
    public static function getFolderPath(): string
    {
        return Kernel::getRootPath() . '/public/content';
    }

    /**
     * @param $type
     *
     * @return string
     */
    public static function getFolderPathForType(string $type): string
    {
        return self::getFolderPath() . '/' . Setting::get("folder/images/$type");
    }
}
