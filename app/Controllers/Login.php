<?php

namespace App\Controllers;

use App\DB;
use App\Models\User;
use App\Router;
use App\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Login
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public static function form(Request $request): string
    {
        return Template::getInstance()->render('login.twig');
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public static function login(Request $request): Response
    {
        $username = $request->get('username');
        $password = $request->get('password');

        $response = new RedirectResponse(Router::route('/login'));

        $userRepository = DB::getInstance()->entityManager->getRepository(User::class);
        $user = $userRepository->findOneBy(['username' => $username]);

        if (is_null($user)) {
            die('NO USER');
        }

        if (password_verify($password, $user->password)) {
            $response->setTargetUrl(Router::route('/'));
            $_SESSION['user'] = $user->id;
        }
        else {
            die('WRONG PASSWORD');
        }

        return $response;
    }

    /**
     * @return RedirectResponse
     */
    public static function logout()
    {
        unset($_SESSION['user']);

        return new RedirectResponse(Router::route('/login'));
    }
}
