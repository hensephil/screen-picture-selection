<?php

namespace App\Controllers;

use App\DB;
use App\Kernel;
use App\Models\Setting;
use App\Models\User;
use App\Router;
use App\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Settings
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public static function index(Request $request): string
    {
        $userRepository = DB::getInstance()->entityManager->getRepository(User::class);

        $context = [
            'folder' => [
                'images' => [
                    'new' => Setting::get('folder/images/new'),
                    'approved' => Setting::get('folder/images/approved'),
                    'denied' => Setting::get('folder/images/denied'),
                ],
            ],
            'email' => [
                'subject' => Setting::get('email/subject'),
                'message' => Setting::get('email/message'),
                'from' => Setting::get('email/from'),
            ],

            'users' => $userRepository->findAll(),
        ];

        return Template::getInstance()->render('settings.twig', $context);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public static function update(Request $request): Response
    {
        $settings = $request->request->all();

        foreach ($settings as $key => $value) {
            $setting = new Setting();
            $setting->key = $key;
            $setting->value = $value;
            DB::getInstance()->save($setting, ['key' => $key]);
        }

        return new RedirectResponse(Router::route('/settings'));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public static function createUser(Request $request): Response
    {
        $user = new User();
        $user->username = $request->get('username');
        $user->password = password_hash($request->get('password'), PASSWORD_DEFAULT);
        $user->email = $request->get('email');
        $user->permission = $request->get('permission');
        DB::getInstance()->save($user);

        // send mail to user
        Kernel::sendMail(Setting::get('email/new_user/from'), $user->email, Setting::get('email/new_user/subject'), Setting::get('email/new_user/message'));

        return new RedirectResponse(Router::route('/settings'));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public static function updateUser(Request $request): Response
    {
        $userRepository = DB::getInstance()->entityManager->getRepository(User::class);
        $user = $userRepository->find($request->get('id'));

        if ($request->get('update')) {
            $user->permission = $request->get('permission');
            DB::getInstance()->save($user);
        }

        elseif ($request->get('delete')) {
            DB::getInstance()->entityManager->createQueryBuilder()
                ->delete(User::class, 'u')
                ->where('u.id = :user_id')
                ->setParameter('user_id', $user->id)
                ->getQuery()->execute();
        }

        return new RedirectResponse(Router::route('/settings'));
    }
}
