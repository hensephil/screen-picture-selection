<?php

namespace App\Controllers;

use App\DB;
use App\Models\Image;
use App\Template;
use Symfony\Component\HttpFoundation\Request;

class Images
{
    public static function view($type)
    {
        $slides = array_map(function ($file) use ($type) {
            $path = Upload::getFolderPathForType($type) . "/$file";
            $imageRepository = DB::getInstance()->entityManager->getRepository(Image::class);
            $image = $imageRepository->findOneBy(['path' => $path]);

            return [
                'url' => self::pathToUrl($path),
                'name' => $file,
                'processedBy' => $image->processedBy ?? '',
            ];
        }, self::readImagesFromFolder(Upload::getFolderPathForType($type)));

        return Template::getInstance()->render('images.twig', [
            'slides' => $slides,
            'type' => $type,
        ]);
    }

    public static function viewNew(): string
    {
        return self::view('new');
    }

    public static function viewApproved(): string
    {
        return self::view('approved');
    }

    public static function viewDenied(): string
    {
        return self::view('denied');
    }

    /**
     * @param Request $request
     */
    public static function approve(Request $request)
    {
        $imageFilename = $request->get('image');
        $sourceType = $request->get('source_type');

        $sourceImagePath = Upload::getFolderPathForType($sourceType) . "/$imageFilename";
        $approvedImagePath = Upload::getFolderPathForType('approved') . "/$imageFilename";

        $image = new Image();
        $image->path = $approvedImagePath;
        $image->status = 'approved';
        $image->processedBy = $_SESSION['user'];
        DB::getInstance()->save($image, ['path' => $sourceImagePath]);

        rename($sourceImagePath, $approvedImagePath);
    }

    /**
     * @param Request $request
     */
    public static function deny(Request $request)
    {
        $imageFilename = $request->get('image');
        $sourceType = $request->get('source_type');

        $sourceImagePath = Upload::getFolderPathForType($sourceType) . "/$imageFilename";
        $deniedImagePath = Upload::getFolderPathForType('denied') . "/$imageFilename";

        $image = new Image();
        $image->path = $deniedImagePath;
        $image->status = 'denied';
        $image->processedBy = $_SESSION['user'];
        DB::getInstance()->save($image, ['path' => $sourceImagePath]);

        rename($sourceImagePath, $deniedImagePath);
    }

    /**
     * @param string $path
     *
     * @return array
     */
    public static function readImagesFromFolder(string $path): array
    {
        return array_values(array_filter(scandir($path), fn ($file) => ! in_array($file, ['.', '..'])));
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public static function pathToUrl(string $path): string
    {
        return str_replace($_SERVER['DOCUMENT_ROOT'], "http://$_SERVER[HTTP_HOST]", $path);
    }
}
