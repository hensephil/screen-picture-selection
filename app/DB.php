<?php

namespace App;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use PetrKnap\Php\Singleton\SingletonTrait;

class DB
{
    use SingletonTrait;

    public EntityManager $entityManager;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/Models'], $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        $conn = array(
            'driver' => 'pdo_sqlite',
            'path' => dirname(__DIR__) . '/db.sqlite',
        );

        $this->entityManager = EntityManager::create($conn, $config);
    }

    /**
     * @param object $entity
     * @param array $criteria
     */
    public function save(object $entity, array $criteria = [])
    {
        $repository = $this->entityManager->getRepository(get_class($entity));
        $exists = $repository->findOneBy($criteria);

        if (! empty($criteria) && ! is_null($exists)) {
            foreach (get_object_vars($entity) as $key => $value) {
                $exists->$key = $value;
            }
        } else {
            $exists = $entity;
        }

        $this->entityManager->persist($exists);
        $this->entityManager->flush();
    }
}
