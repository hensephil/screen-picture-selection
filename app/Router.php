<?php

namespace App;

use App\Controllers\Account;
use App\Controllers\Images;
use App\Controllers\Settings;
use App\Models\User;
use App\Controllers\Dashboard;
use App\Controllers\Login;
use App\Controllers\Upload;
use PetrKnap\Php\Singleton\SingletonTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Router
{
    use SingletonTrait;

    public array $routes = [
        'GET' => [
            '/login' => [Login::class, 'form'],
            '/logout' => [Login::class, 'logout'],

            '/upload' => [Upload::class, 'form'],

            '/' => [Dashboard::class, 'dashboard'],

            '/images/new' => [Images::class, 'viewNew'],
            '/images/approved' => [Images::class, 'viewApproved'],
            '/images/denied' => [Images::class, 'viewDenied'],

            '/account' => [Account::class, 'index'],

            '/settings' => [Settings::class, 'index'],
        ],

        'POST' => [
            '/login' => [Login::class, 'login'],

            '/upload' => [Upload::class, 'upload'],

            '/images/approve' => [Images::class, 'approve'],
            '/images/deny' => [Images::class, 'deny'],

            '/account/update' => [Account::class, 'update'],

            '/settings/update' => [Settings::class, 'update'],
            '/settings/user/create' => [Settings::class, 'createUser'],
            '/settings/user/update' => [Settings::class, 'updateUser'],
        ],

        'guest' => [
            '/login',
            '/upload',
        ],

        'permissions' => [
            '/settings' => 100,
            '/settings/update' => 100,
            '/settings/user/create' => 100,
            '/settings/user/update' => 100,
        ],
    ];

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $request = Request::createFromGlobals();
        $response = new Response();

        if (! $this->checkPermission($request)) {
            (new RedirectResponse(self::route('/login')))->send();
            exit;
        }

        $method = $request->getMethod();
        $path = self::getRealPathInfo($request);
        if (isset($this->routes[$method][$path])) {
            $content = call_user_func($this->routes[$method][$path], $request);

            if ($content instanceof Response) {
                $response = $content;
            } else {
                $response->setContent($content);
            }
        } else {
            $response->setStatusCode(404);
            $response->setContent('Not Found');
        }

        $response->send();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function checkPermission(Request $request): bool
    {
        $path = self::getRealPathInfo($request);

        if (in_array($path, $this->routes['guest'])) {
            return true;
        }

        $user = User::getFromSession();
        if (is_null($user)) {
            return false;
        }

        if ($user->permission < ($this->routes['permissions'][$path] ?? 0)) {
            return false;
        }

        return true;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public static function getRealPathInfo(Request $request): string
    {
        return str_replace(Kernel::getPublicRelativePath(), '', $request->getPathInfo()) ?: '/';
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public static function route(string $path): string
    {
        return Kernel::getPublicRelativePath() . $path;
    }
}
