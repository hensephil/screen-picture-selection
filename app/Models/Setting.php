<?php

namespace App\Models;

use App\DB;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="settings")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public int $id;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    public string $key;

    /**
     * @ORM\Column(type="string")
     */
    public string $value;

    /**
     * @param string $key
     *
     * @return string
     */
    public static function get(string $key): string
    {
        $setting = DB::getInstance()->entityManager->getRepository(Setting::class)->findOneBy(['key' => $key]);

        return $setting ? $setting->value : '';
    }
}
