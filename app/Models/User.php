<?php

namespace App\Models;

use App\DB;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public int $id;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    public string $username;

    /**
     * @ORM\Column(type="string")
     */
    public string $password;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    public string $email;

    /**
     * @ORM\Column(type="integer")
     */
    public int $permission;

    /**
     * @return User|object
     */
    public static function getFromSession()
    {
        return DB::getInstance()->entityManager->find(User::class, $_SESSION['user'] ?? 0);
    }
}
