<?php

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="images")
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public int $id;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    public string $path;

    /**
     * @ORM\Column(type="string")
     */
    public string $status;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\Column(type="integer",nullable=true)
     */
    public $processedBy;
}
