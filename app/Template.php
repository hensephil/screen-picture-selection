<?php

namespace App;

use App\Controllers\Images;
use App\Controllers\Upload;
use App\Models\User;
use PetrKnap\Php\Singleton\SingletonTrait;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class Template
{
    use SingletonTrait;

    public Environment $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader(dirname(__DIR__) . '/templates');
        $this->twig = new Environment($loader, [
            #'cache' => dirname(__DIR__) . '/tmp/cache/twig',
        ]);

        $this->twig->addFunction(new TwigFunction('route', [Router::class, 'route']));
    }

    /**
     * @param string $name
     * @param array $context
     *
     * @return string
     */
    public function render(string $name, array $context = []): string
    {
        $context['images'] = [
            'new' => Images::readImagesFromFolder(Upload::getFolderPathForType('new')),
            'approved' => Images::readImagesFromFolder(Upload::getFolderPathForType('approved')),
            'denied' => Images::readImagesFromFolder(Upload::getFolderPathForType('denied')),
        ];

        $context['user'] = User::getFromSession();

        return $this->twig->render($name, $context);
    }
}
