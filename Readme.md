## Requirements

- PHP >= 7.4
  - php-sqlite3
- composer
- Webserver
  - set webroot to `public` directory
- Postfix
- Ordner für die Bilder im `public/content` Ordner


## Install Instructions

```
git clone $url
cd $directory
composer install
# vendor/bin/doctrine orm:schema-tool:drop --force
vendor/bin/doctrine orm:schema-tool:create
sqlite3 db.sqlite
INSERT INTO users (username, password, email, permission) VALUES ("admin", "$2y$10$J4OmbCVSSkb7duz9kLme2e1Cm6wcNfgvfrwZ5/7Sh7H4nqH58UEaG", "admin@example.org", 100);
INSERT INTO settings (key, value) VALUES ("folder/images/new", "new");
INSERT INTO settings (key, value) VALUES ("folder/images/approved", "approved");
INSERT INTO settings (key, value) VALUES ("folder/images/denied", "denied");
mkdir public/content/new
mkdir public/content/approved
mkdir public/content/denied
```
