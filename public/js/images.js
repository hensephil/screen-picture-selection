const splide = new Splide('.splide').mount();

const buttonApprove = document.querySelector('#button-approve');
const buttonDeny = document.querySelector('#button-deny');

if (buttonApprove) {
  buttonApprove.addEventListener('click', (ev) => {
    const index = splide.index;
    const slide = document.querySelectorAll('.splide__slide')[index];
    const image = slide.querySelector('img');

    const data = new FormData();
    data.append('image', image.dataset.image);
    data.append('source_type', source_type);

    fetch(GLOBAL.PUBLIC_PATH + '/images/approve', {
      method: 'POST',
      body: data,
    });

    splide.remove(index);
  });
}

if (buttonDeny) {
  buttonDeny.addEventListener('click', (ev) => {
    const index = splide.index;
    const slide = document.querySelectorAll('.splide__slide')[index];
    const image = slide.querySelector('img');

    const data = new FormData();
    data.append('image', image.dataset.image);
    data.append('source_type', source_type);

    fetch(GLOBAL.PUBLIC_PATH + '/images/deny', {
      method: 'POST',
      body: data,
    });

    splide.remove(index);
  });
}
